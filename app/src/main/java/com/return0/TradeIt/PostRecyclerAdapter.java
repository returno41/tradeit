package com.return0.TradeIt;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.List;

public class PostRecyclerAdapter extends RecyclerView.Adapter<PostRecyclerAdapter.ViewHolder> {

private Context context;
private List<Trader> list;
private RecyclerViewOnclickListener listener;

public PostRecyclerAdapter(){ }

public PostRecyclerAdapter(Context context, List<Trader> list,RecyclerViewOnclickListener listener){
    this.context=context;
    this.list=list;
    this.listener=listener;
}

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.postrecyclerview,parent,false);
        final ViewHolder viewHolder=new ViewHolder(view);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.itemclick(view,viewHolder.getAdapterPosition());
            }
        });
        return viewHolder;
    }

    @SuppressLint("LogNotTimber")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
Trader tradeIt=list.get(position);
holder.itemName.setText(tradeIt.getPost_title());
holder.itemDescription.setText(tradeIt.getPost_desc());
//Log.e("description",tradeIt.getDesc());
holder.itemPrice.setText(tradeIt.getPrice());
holder.imageurl.setText(tradeIt.getPost_url());
holder.imageuid.setText(tradeIt.getUid());
holder.postid.setText(tradeIt.getPostid());

Glide.with(context).load(tradeIt.getPost_url()).into(holder.imageView);

    }

    @Override
    public int getItemCount() {
      return list.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{

TextView itemName,itemDescription,itemPrice,imageurl,imageuid,postid;
ImageView imageView;

ViewHolder(View itemview){
    super(itemview);

    postid=itemview.findViewById(R.id.postid);
    itemName=itemview.findViewById(R.id.title);
    itemDescription=itemview.findViewById(R.id.descrip);
    itemPrice=itemview.findViewById(R.id.price);
    imageurl=itemview.findViewById(R.id.imageurl);
    imageView=itemview.findViewById(R.id.postImg);
    imageuid=itemview.findViewById(R.id.imaeuid);

}


    }

}
