package com.return0.TradeIt;
class Trader {

    private String post_desc, post_title, post_url,price,uid,postid;

    public Trader(String post_desc, String post_title, String post_url, String price, String uid,String postid) {
        this.post_title = post_title;
        this.post_desc = post_desc;
        this.post_url =post_url;
        this.uid = uid;
        this.price=price;
        this.postid=postid;
    }

    public Trader() {
    }

    public String getPrice(){
        return price;
    }

    public void setPostid(String postid) {
        this.postid = postid;
    }

    public void setPost_url(String post_url) {
        this.post_url = post_url;
    }

     public void setPost_title(String post_title) {
        this.post_title = post_title;
    }

    public void setPost_desc(String post_desc) {
        this.post_desc = post_desc;
    }

    public String getPostid() {
        return postid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getUid() {
        return uid;
    }

    public String getPost_url() {
        return post_url;
    }

    public String getPost_title() {
        return post_title;
    }

    public String getPost_desc() {
        return post_desc;
    }

}
