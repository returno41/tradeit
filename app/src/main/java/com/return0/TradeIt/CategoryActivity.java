package com.return0.TradeIt;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.plattysoft.leonids.ParticleSystem;

import java.util.ArrayList;
import java.util.List;

public class CategoryActivity extends AppCompatActivity  {
    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    private List<Trader> tradeItList;
    private AlertDialog.Builder builder;
    private String category;

    @SuppressLint("LogNotTimber")
    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.categories_activity);


        TextView categ=findViewById(R.id.categorytext);
        Toolbar toolbar=findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar()!=null){
            getSupportActionBar().setTitle("");
        }

        //get the appropriate category
        Intent intent=getIntent();
        category=intent.getStringExtra("category");

        Log.e("Category",category);
        if (category==null){
            return;
        }
        categ.setText(category);
        tradeItList=new ArrayList<>();
        recyclerView=findViewById(R.id.category_recycler);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
builder=new AlertDialog.Builder(CategoryActivity.this);
       builder.setView(R.layout.progressdialog);
        final Dialog dialog=builder.create();
        dialog.show();

        DatabaseReference reference=FirebaseDatabase.getInstance().getReference("Posts");
        reference.child(category).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
for(DataSnapshot dataSnapshot1:dataSnapshot.getChildren()){
   // String s=dataSnapshot1.child("post_desc").getValue().toString();
   // Log.e("postid ",s);
    Trader tradeIt=dataSnapshot1.getValue(Trader.class);
    tradeItList.add(tradeIt);

}
     adapter=new PostRecyclerAdapter(getApplicationContext(), tradeItList, new RecyclerViewOnclickListener() {
         @Override
         public void itemclick(View view, int position) {
    gotoSingleItem(view);
         }
     });
recyclerView.setAdapter( adapter);
dialog.dismiss();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
dialog.dismiss();

            }
        });


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater=getMenuInflater();
        inflater.inflate(R.menu.categorymenu,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
       int id=item.getItemId();

       if (id==R.id.logout){
          //signout user
           FirebaseAuth.getInstance().signOut();
           startActivity(new Intent(this,LoginActivity.class));
       }else
           if (id==R.id.newPost){

               //Go to a new post activity
               Intent intent=new Intent(CategoryActivity.this,CategoryPostActivity.class);
               intent.putExtra("category",category);
               startActivity(intent);
           }
           else {
               super.onOptionsItemSelected(item);
           }

       return true;
    }

    private void gotoSingleItem(View view){
    TextView uid,image,posttitle,postdesc,price,postid;
    uid=view.findViewById(R.id.imaeuid);
    image=view.findViewById(R.id.imageurl);
    posttitle=view.findViewById(R.id.title);
    postdesc=view.findViewById(R.id.descrip);
    price=view.findViewById(R.id.price);
    postid=view.findViewById(R.id.postid);

    final String userId,imageUr,postTitle,postDesc,pricey,PostId;
    userId=uid.getText().toString();
    imageUr=image.getText().toString();
    postTitle=posttitle.getText().toString();
    postDesc=postdesc.getText().toString();
    pricey=price.getText().toString();
    PostId=postid.getText().toString();

    Log.e("Userid",userId);

    new ParticleSystem(CategoryActivity.this,100,R.drawable.fireworksanim,4000)
            .setSpeedRange(0.1f,0.25f)
            .setRotationSpeedRange(90,180)
            .setInitialRotationRange(0,360)
            .oneShot(view.findViewById(R.id.emiter),100);



    new Handler().postDelayed(new Runnable() {
        @Override
        public void run() {
            Intent intent=new Intent(CategoryActivity.this,SingLeItemActivity.class);
            intent.putExtra("userid",userId);
            intent.putExtra("imageurl",imageUr);
            intent.putExtra("title",postTitle);
            intent.putExtra("descrip",postDesc);
            intent.putExtra("price",pricey);
            intent.putExtra("postid",PostId);
            intent.putExtra("category",category);
            startActivity(intent);



        }
    },4000);


    }
}
