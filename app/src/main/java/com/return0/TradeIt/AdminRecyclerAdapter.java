package com.return0.TradeIt;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.List;

public class AdminRecyclerAdapter extends RecyclerView.Adapter<AdminRecyclerAdapter.ViewHolder> {

private Context context;
private List<TradeIt> list;
private RecyclerViewOnclickListener listener;

    AdminRecyclerAdapter(Context context, List<TradeIt> list, RecyclerViewOnclickListener listener){
    this.context=context;
    this.list=list;
    this.listener=listener;
}

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.recycleritem,parent,false);
        final ViewHolder viewHolder=new ViewHolder(view);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.itemclick(view,viewHolder.getAdapterPosition());
            }
        });
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
TradeIt tradeIt=list.get(position);
holder.categoryName.setText(tradeIt.getPost_title());
holder.categoryId.setText(tradeIt.getDesc());
Glide.with(context).load(tradeIt.getPost_url()).into(holder.imageView);

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{

TextView categoryName,categoryId;
ImageView imageView;

ViewHolder(View itemview){
    super(itemview);

    categoryName=itemview.findViewById(R.id.categ);
    categoryId=itemview.findViewById(R.id.desc);
    imageView=itemview.findViewById(R.id.categImg);
}


    }

}
