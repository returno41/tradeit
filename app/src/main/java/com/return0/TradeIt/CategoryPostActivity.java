package com.return0.TradeIt;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.ContentResolver;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;

//import android.support.annotation.NonNull;
//import android.support.v7.app.AppCompatActivity;
//import com.squareup.picasso.Picasso;

@SuppressWarnings("ConstantConditions")
public class CategoryPostActivity extends AppCompatActivity {
    // imports
    private ImageView imageBtn;
    private static final int GALLERY_REQUEST_CODE = 2;
    private Uri filePath = null;
    private TextInputEditText textTitle;
    private TextInputEditText textDesc;
    private String downloadUrl;
    private StorageReference storage;
    private FirebaseAuth mAuth;
    private FirebaseUser mCurrentUser;
    private AlertDialog.Builder builder;
    private Dialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_post);
        Intent intent=getIntent();
        final String category=intent.getStringExtra("category");
        if (category == null){
            return;
        }

        // initializing objects
        Button postBtn = findViewById(R.id.postBtn);
        textDesc = findViewById(R.id.textDesc);
        textTitle = findViewById(R.id.textTitle);
        final TextInputEditText price=findViewById(R.id.itemprice);
        price.setVisibility(View.VISIBLE);

        //get reference to the firebase firestore
        storage = FirebaseStorage.getInstance().getReference("Posts");

        //Get an instance of the current user
        mCurrentUser = FirebaseAuth.getInstance().getCurrentUser();

        //Get the current username
       FirebaseDatabase.getInstance().getReference().child("Users").child(mCurrentUser.getUid());
        imageBtn = findViewById(R.id.imageBtn);

        //<editor-fold defaultstate="collapsed" desc="picking image from gallery">
        imageBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent galleryIntent = new Intent(Intent.ACTION_GET_CONTENT);
                galleryIntent.setType("image/*");
                startActivityForResult(Intent.createChooser(galleryIntent, "Choose an Image"), GALLERY_REQUEST_CODE);
            }
        });
        // </editor-fold>

        //< editor-fold desc=" posting the image and details to firebase to Firebase">
        postBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String PostTitle = textTitle.getText().toString().trim();
                final String PostDesc = textDesc.getText().toString().trim();

                // do a check for empty fields
                if (!TextUtils.isEmpty(PostDesc) && !TextUtils.isEmpty(PostTitle)) {

                    //Show a progress dialog
                    builder=new AlertDialog.Builder(CategoryPostActivity.this);
                    builder.setView(R.layout.progressdialog);
                    dialog=builder.create();
                    dialog.setCanceledOnTouchOutside(false);
                    dialog.show();
                    if (filePath==null) {
                        Toast.makeText(CategoryPostActivity.this,"Please select an Image. ",Toast.LENGTH_LONG).show();
                        dialog.dismiss();
                        return;
                    }

                    //Upload the image to firebase firestore
                    final StorageReference Sref= storage.child(category).child(System.currentTimeMillis()+"."+getFileExtension(filePath));

                   UploadTask uploadTask= Sref.putFile(filePath);

                    Task<Uri> uriTask=uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
                        @Override
                        public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                            if (!task.isSuccessful()){
                                Toast.makeText(CategoryPostActivity.this,"Could not upload File "+task.getException().getMessage(),Toast.LENGTH_LONG
                                ).show();
                            }
                            return Sref.getDownloadUrl();
                        }
                    }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                        @SuppressLint("LogNotTimber")
                        @Override
                        public void onComplete(@NonNull Task<Uri> task) {

                            //getting the post image download url
                            if(task.isSuccessful()) {
                                Uri downloadUri = task.getResult();
                                downloadUrl = downloadUri.toString();

                                Log.e("Download Url", downloadUrl);

                                //  final Uri downloadUrl = null;
                                Toast.makeText(getApplicationContext(), "Succesfully Uploaded", Toast.LENGTH_SHORT).show();
                                final DatabaseReference newPost = FirebaseDatabase.getInstance().getReference("Posts").child(category).push();
                                //adding post contents to database reference
                                newPost.addValueEventListener(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(@NotNull DataSnapshot dataSnapshot) {
                                        newPost.child("post_title").setValue(PostTitle);
                                        newPost.child("post_desc").setValue(PostDesc);
                                        newPost.child("post_url").setValue(downloadUrl);
                                        newPost.child("uid").setValue(mCurrentUser.getUid());
                                        newPost.child("postid").setValue(newPost.getKey());
                                        newPost.child("price").setValue(price.getText().toString())
                                                .addOnCompleteListener(new OnCompleteListener<Void>() {
                                                    @Override
                                                    public void onComplete(@NonNull Task<Void> task) {

                                                        if (task.isSuccessful()) {
                                                            //Everything ok go to main activity
                                                            dialog.dismiss();
                                                            Intent intent = new Intent(CategoryPostActivity.this, MainActivity.class);
                                                            startActivity(intent);
                                                        }
                                                    }
                                                });
                                    }

                                    @Override
                                    public void onCancelled(@NotNull DatabaseError databaseError) {

                                    }
                                });

                            }
                        }
                    });

                }
            }
        });

    }

    //<editor-fold desc="Getting the image from gallery">
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == GALLERY_REQUEST_CODE && resultCode == RESULT_OK && data != null && data.getData() != null) {
            filePath = data.getData();
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);
                imageBtn.setImageBitmap(bitmap);
                TextView textView=findViewById(R.id.textview);
                textView.setVisibility(View.INVISIBLE);
                imageBtn.setScaleType(ImageView.ScaleType.CENTER_CROP);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
    //</editor-fold>

 //   <editor-fold desc="getting the image extension inorder to rename it">

    private String getFileExtension(Uri uri) {
        ContentResolver cR = getContentResolver();
        MimeTypeMap mime = MimeTypeMap.getSingleton();
        return mime.getExtensionFromMimeType(cR.getType(uri));
    }
//</editor-fold>

}
