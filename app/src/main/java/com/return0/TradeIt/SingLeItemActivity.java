package com.return0.TradeIt;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.DexterError;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.PermissionRequestErrorListener;
import com.karumi.dexter.listener.single.PermissionListener;

public class SingLeItemActivity extends AppCompatActivity {
    private String Username;
    private String Userphone,imageUrl;
    private String PostId;
    private String Category;
private TextView User,postid;
    //private AlertDialog.Builder builder;
private Dialog dialog=null;
AlertDialog.Builder builder;
private BroadcastReceiver sentStatus,deleveredStatus;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sing_le_item);
        User=findViewById(R.id.poster);
        TextView title = findViewById(R.id.singTitle);
        TextView desc = findViewById(R.id.singDesc);
        TextView price = findViewById(R.id.singPrice);
        ImageView image = findViewById(R.id.singImage);
        BottomNavigationView bottomNavigationView=findViewById(R.id.nav_view);
        bottomNavigationView.setBackground(ContextCompat.getDrawable(getApplicationContext(),R.drawable.bottombac));
        bottomNavigationView.setItemIconTintList(ContextCompat.getColorStateList(getApplicationContext(),R.color.colorwhite));

        Intent intent=getIntent();
        String userId = intent.getStringExtra("userid");
        imageUrl = intent.getStringExtra("imageurl");
        String title1 = intent.getStringExtra("title");
        String description = intent.getStringExtra("descrip");
        String pricey = intent.getStringExtra("price");
        PostId=intent.getStringExtra("postid");
        Category=intent.getStringExtra("category");

        Log.e("userid",userId);
        title.setText(title1);
        desc.setText(description);
        price.setText(pricey);
        Glide.with(SingLeItemActivity.this).load(imageUrl).into(image);
        String currentUser= FirebaseAuth.getInstance().getCurrentUser().getUid();
        if (!currentUser.equals(userId)){

            bottomNavigationView.findViewById(R.id.Delete).setVisibility(View.INVISIBLE);
        }

        assert userId != null;
        FirebaseDatabase.getInstance().getReference("AllUsers").child(userId).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
             Username=dataSnapshot.child("Username").getValue().toString();

             User.setText("Posted By: "+Username);

             Userphone=dataSnapshot.child("phone").getValue().toString();




            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
          int i=menuItem.getItemId();
          if(i==R.id.buy){
         buyItem() ;

          }else
              if(i==R.id.Delete){
                  deleteItem();
              }



                return false;
            }
        });
    }

    private void buyItem(){
        LayoutInflater inflater=LayoutInflater.from(SingLeItemActivity.this) ;
        View view=inflater.inflate(R.layout.contactseller,null,false);
    builder=new AlertDialog.Builder(SingLeItemActivity.this)  ;
   builder.setTitle("Contact Seller");
   builder.setView(view);
   dialog=builder.create();
   dialog.setCanceledOnTouchOutside(false);

        final TextInputEditText mess;
        Button buy,cancel;
   mess= view.findViewById(R.id.message)  ;
   buy=view.findViewById(R.id.buy);
   cancel=view.findViewById(R.id.cancel);

   buy.setOnClickListener(new View.OnClickListener() {
       @Override
       public void onClick(View view) {
      String sms=mess.getText().toString();

      if (sms!=""){
           sendMessage(sms,dialog);}
      return;
       }
   });

   cancel.setOnClickListener(new View.OnClickListener() {
       @Override
       public void onClick(View view) {
           dialog.dismiss();
       }
   });

   dialog.show();
    }

    private void sendMessage(final String sms, final Dialog dialog){
       dialog.setContentView(R.layout.progressdialog);

        Dexter.withActivity(SingLeItemActivity.this)
                .withPermission(Manifest.permission.SEND_SMS)
                .withListener(new PermissionListener() {
                    @Override
                    public void onPermissionGranted(PermissionGrantedResponse response) {
                        Log.e("Phone",Userphone);

                        PendingIntent sentIntent=PendingIntent.getBroadcast(getApplicationContext(),0,new Intent("SMS_SENT"),0);
                        PendingIntent deliveredIntent=PendingIntent.getBroadcast(getApplicationContext(),0,new Intent("SMS_DELIVERED"),0);

                        SmsManager manager=SmsManager.getDefault();
                        manager.sendTextMessage(Userphone,null,"TradeIt "+sms,sentIntent,deliveredIntent);
dialog.dismiss();

                    }

                    @Override
                    public void onPermissionDenied(PermissionDeniedResponse response) {
          if (response.isPermanentlyDenied()) {
AlertDialog.Builder builder=new AlertDialog.Builder(SingLeItemActivity.this);
builder.setTitle("Please Grant Permission");
builder.setMessage(getString(R.string.prompt));
builder.setPositiveButton("Take Me To Settings", new DialogInterface.OnClickListener() {
    @Override
    public void onClick(DialogInterface dialogInterface, int i) {
        dialogInterface.cancel();
        Intent intent=new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri=Uri.fromParts("package",getPackageName(),null);
        intent.setData(uri);
        startActivityForResult(intent,101);
    }
});
builder.setNegativeButton("Exit", new DialogInterface.OnClickListener() {
    @Override
    public void onClick(DialogInterface dialogInterface, int i) {
    finishAffinity();
    System.exit(0);
    }
});
builder.show();



          }

                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {
token.continuePermissionRequest();
                    }
                }).withErrorListener(new PermissionRequestErrorListener() {
            @Override
            public void onError(DexterError error) {

            }
        }).check();

    }

    public void onResume(){
        super.onResume();
        sentStatus=new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
         String s="An Error Occured" ;
         switch (getResultCode()){
             case Activity
                  .RESULT_OK  :s="Message Sent" ;
             break;
             case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
                 s="Genereic failure";
                 break;
                 default:s="Sms Not Sent";
                 break;
         }
                Toast.makeText(getApplicationContext(),s,Toast.LENGTH_LONG).show(); ;

            }
        };

        deleveredStatus=new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String s="An Error Occured" ;
                switch (getResultCode()){
                    case Activity
                            .RESULT_OK  :s="Message Sent" ;
                        break;
                    case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
                        s="Genereic failure";
                        break;
                    default:s="Sms Not Sent";
                        break;
                }
                Toast.makeText(getApplicationContext(),s,Toast.LENGTH_LONG).show();
            }
        };

        registerReceiver(sentStatus,new IntentFilter("SMS_SENT"));
        registerReceiver(deleveredStatus,new IntentFilter("SMS_RECEIVED"));
    }

    public void onPause(){
        super.onPause();
        unregisterReceiver(sentStatus);
        unregisterReceiver(deleveredStatus);
    }

    private void deleteItem(){
        dialog=new Dialog(SingLeItemActivity.this);
        dialog.setContentView(R.layout.progressdialog);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    FirebaseDatabase.getInstance().getReference("Posts").child(Category).child(PostId).removeValue().addOnSuccessListener(new OnSuccessListener<Void>() {
        @Override
        public void onSuccess(Void aVoid) {

           // StorageReference reference= FirebaseStorage.getInstance().getReference(imageUrl) ;
           /* reference.delete().addOnSuccessListener(new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void aVoid) {

                }
            });*/
           dialog.dismiss();
            Toast.makeText(getApplicationContext(),"SuccessFully Deleted",Toast.LENGTH_LONG).show();
            startActivity(new Intent(SingLeItemActivity.this,MainActivity.class));
        }
    });

    }
}
