package com.return0.TradeIt;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatEditText;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.karumi.dexter.BuildConfig;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import timber.log.Timber;

@SuppressWarnings("ConstantConditions")
public class RegisterActivity extends AppCompatActivity {

    private AppCompatEditText UserText,UserMailText,PassWord1Text,Password2Text,PhoneText;
    private String User,email,password1,password2,phone,UserId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

    //getSupportActionBar().hide();
    if (BuildConfig.DEBUG){
        Timber.plant(new Timber.DebugTree());
    }

UserText=findViewById(R.id.RegisterName);
UserMailText=findViewById(R.id.RegisterEmail);
PassWord1Text=findViewById(R.id.RegisterPassword);
Password2Text=findViewById(R.id.RegisterPasswordConfirm);
PhoneText=findViewById(R.id.RegisterPhone);

        Button login = findViewById(R.id.RegisterLogin);
        Button register = findViewById(R.id.RegisterRegister);

login.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
        Intent intent=new Intent(RegisterActivity.this,LoginActivity.class);
        startActivity(intent);
    }
});

register.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
        User=UserText.getText().toString().trim();
        email=UserMailText.getText().toString().trim();
        password1=PassWord1Text.getText().toString().trim();
        password2=Password2Text.getText().toString().trim();
        phone=PhoneText.getText().toString().trim();


if(isValidUserName(User)){
    if(isValidEmail(email)){
      if (isValidPass(password1,password2)){
          if (isValidPhone(phone)){
              registerUser(User,email,phone,password1);
          }else{
          PhoneText.setError("The phone number must begin with a 0 and contain only 10 digits");
          }
      }else{
      PassWord1Text.setError("Use more than 4 characters for password");
      }
    }else {
        UserMailText.setError("Wrong email format");
    }

}else {
    UserText.setError("Wrong Username Format Use 4-6 characters with both lower and capital letters");
}
    }
});
    }

    private boolean isValidUserName(String s){
        String regex="^[a-zA-Z].{4,6}";
        Pattern pattern=Pattern.compile(regex);
        Matcher matcher=pattern.matcher(s);
        return matcher.matches();
    }

    private boolean isValidEmail(String email){
        String regex="^.+@.+\\..+$";
        Pattern pattern=Pattern.compile(regex);
        Matcher matcher=pattern.matcher(email);
        return matcher.matches();
    }

    private boolean isValidPass(String password1, String password2){
        return password1.equals(password2) && password1.length() > 4;
    }

    private boolean isValidPhone(String s){
        //Scanner scanner=new Scanner(s);
        return s.charAt(0)=='0' && s.length()==10 ;
    }

    private void registerUser(final String name, final String email, final String phone, final String password){

        AlertDialog.Builder builder=new AlertDialog.Builder(this);
        builder.setView(R.layout.progressdialog);
        final Dialog dialog=builder.create();
        dialog.show();

        final FirebaseAuth auth=FirebaseAuth.getInstance();
        auth.createUserWithEmailAndPassword(email,password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
               auth.signInWithEmailAndPassword(email,password).addOnSuccessListener(new OnSuccessListener<AuthResult>() {
                   @Override
                   public void onSuccess(AuthResult authResult) {

                   }
               }).addOnFailureListener(new OnFailureListener() {
                   @Override
                   public void onFailure(@NonNull Exception e) {
                    dialog.dismiss();

                   }
               });


                UserId=auth.getCurrentUser().getUid();
               String usersDb="AllUsers" ;
                FirebaseDatabase database=FirebaseDatabase.getInstance();
                final DatabaseReference reference=database.getReference(usersDb).child(UserId);

                reference.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                      reference.child("Username").setValue(name) ;
                      reference.child("phone").setValue(phone);
                      reference.child("id").setValue(UserId);

                      //Timber.e("Generated user Id %s", userId);
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
            }
        }).addOnSuccessListener(new OnSuccessListener<AuthResult>() {
            @Override
            public void onSuccess(AuthResult authResult) {
                dialog.dismiss();
                Toast.makeText(RegisterActivity.this,"Registration Successful",Toast.LENGTH_LONG) .show();
                Intent intent=new Intent(RegisterActivity.this,LoginActivity.class);
                startActivity(intent);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(RegisterActivity.this,"Cannot Register "+ e.getMessage(),Toast.LENGTH_LONG).show();
                dialog.dismiss();
            }
        });
    }
}
