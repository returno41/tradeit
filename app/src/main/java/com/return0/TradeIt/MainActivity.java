package com.return0.TradeIt;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;
import android.widget.ViewFlipper;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    private Dialog dialog;
    private List<TradeIt> tradeIts;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //hide the default actionB

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null)
            getSupportActionBar().setTitle("");


        Animation animin = AnimationUtils.loadAnimation(this, R.anim.animin);
        Animation animout = AnimationUtils.loadAnimation(this, R.anim.animout);

        ViewFlipper flipper = findViewById(R.id.flipper);
        flipper.setFlipInterval(5000);
        flipper.setAutoStart(true);
        flipper.setInAnimation(animin);
        flipper.setOutAnimation(animout);
        flipper.startFlipping();

        recyclerView = findViewById(R.id.recycler);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new GridLayoutManager(this, 2));

        tradeIts = new ArrayList<>();

        //Progress Dialog
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(R.layout.progressdialog);
        dialog = builder.create();
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        DatabaseReference reference = FirebaseDatabase.getInstance().getReference("Categories");
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {
                    TradeIt tradeIt = dataSnapshot1.getValue(TradeIt.class);
                    tradeIts.add(tradeIt);

                }

                adapter = new AdminRecyclerAdapter(getApplicationContext(), tradeIts, new RecyclerViewOnclickListener() {

                    @Override
                    public void itemclick(View view, int position) {

                        gotoSingleCategory(view);

                    }
                });
                recyclerView.setAdapter(adapter);

                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
       }
        });


    }

    private void gotoSingleCategory(View view) {
        TextView categoryId;
        String category;
        categoryId = view.findViewById(R.id.categ);
        category = categoryId.getText().toString();

        Intent intent = new Intent(MainActivity.this, CategoryActivity.class);
        intent.putExtra("category", category);
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu1, menu);

       //Get email of currentuser
        FirebaseUser user=FirebaseAuth.getInstance().getCurrentUser();
        if (user != null) {
           String email=user.getEmail();

           //Check whether the user is currently admin
            if (("b83794444@gmail.com").equals(email)){
                menu.findItem(R.id.add).setVisible(true);
                this.invalidateOptionsMenu();
            }
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
      int id=item.getItemId();
      if (id==R.id.logout1){
          //logout
          FirebaseAuth.getInstance().signOut();
          startActivity(new Intent(this,LoginActivity.class));

      }
      else
          if(id==R.id.add){
              //Go to adminpost activity
            startActivity(new Intent(this,AdminPostActivity.class));
          }

       return true;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

}
